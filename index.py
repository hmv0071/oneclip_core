import cv2
import numpy as np
import moviepy.editor as mp
import speech_recognition as sr
from audio_to_text import audio_to_timed_text
from pydub import AudioSegment

# import os
# from gtts import gTTS
# import math


class VideoEditor:
    def __init__(self, video_file_path):
        self.video_file = video_file_path
        self.video = cv2.VideoCapture(video_file_path)
        self.fps = self.video.get(cv2.CAP_PROP_FPS)
        self.width = int(self.video.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    def extract_audio(self, output_file):
        # Extract audio using moviepy
        audio = mp.AudioFileClip(self.video_file)
        audio.write_audiofile(output_file)

    def audio_to_timed_text(self, audio_file_path):
        sound = AudioSegment.from_file(output_audio_file)
        sound = sound.set_channels(1)
        sound.export(output_audio_file, format="wav")

        list_of_words = audio_to_timed_text(audio_file_path)

        for word in list_of_words:
            print(word.to_string())


video_editor = VideoEditor("assets/sample_video_1.mp4")

output_audio_file = "original_audio.wav"
video_editor.extract_audio(output_audio_file)

video_editor.audio_to_timed_text(output_audio_file)
